Comment protéger une branche dans Gitlab ?

Accédez à votre projet dans GitLab.
Allez dans Paramètres > Dépôt > Branches protégées.
Sélectionnez la branche, définissez les niveaux de protection et cliquez sur Protéger.


Pourquoi protéger des branches dans Git ?

Protéger des branches dans Git permet de prévenir les modifications non autorisées, garantir le passage par des revues de code pour maintenir la qualité, et respecter les workflows établis, comme les pull requests et les tests automatisés.


Qu'est ce qui peut entraîner des merge conflicts et comment les résoudre ?

Causes des conflits :
•	Modifications des mêmes lignes dans des fichiers par différentes branches.
•	Une branche supprime un fichier qu'une autre branche a modifié.

Résolution des conflits :
-	Utiliser git status pour identifier les fichiers en conflit.
-	Ouvrir les fichiers en conflit et chercher les marques de conflit (<<<<<<<, =======, >>>>>>>).
-	Modifier le contenu pour résoudre le conflit, en choisissant les modifications à garder ou en combinant les changements.
-	Marquer le conflit comme résolu avec git add [nom du fichier].
-	Finaliser la fusion par un git commit.

